<div class="documents-categories">
	<div class="sticky-wrapper">
		<div class="section-header">
			<h4>Categories</h4>
		</div>

		<div class="categories-list">
			<h5>Jump to:</h5>

			<?php if(have_rows('documents_categories')): ?>

				<ul>
					<?php while(have_rows('documents_categories')): the_row(); ?>
				 
						<li>
							<a href="#<?php echo get_sub_field('slug'); ?>" data-cat="<?php echo get_sub_field('slug'); ?>">
								<?php echo get_sub_field('name'); ?>
							</a>
						</li>
			
					<?php endwhile; ?>
				</ul>
			<?php endif; ?>

		</div>			
	</div>
</div>