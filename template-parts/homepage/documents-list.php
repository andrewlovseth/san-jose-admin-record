<?php 

$spreadsheet_id = get_field('sheet_id');
$tab_id = get_field('tab_id');
$base_url = get_field('document_base_url');
$today_date = date('Ymd');

if(get_transient('document_list')) {
    $entries = get_transient('document_list');
} else {
    $entries = esa_get_sheet_data('document_list', $spreadsheet_id, MONTH_IN_SECONDS, $tab_id);
}

?>

<div class="documents-list">
	<table id="documents-table">
		<thead>
			<tr>
				<th class="cat">Category</th>
				<th class="date">Date</th>
				<th class="title">Title</th>
				<th class="author">Author</th>
				<th class="pdf">File</th>
			</tr>
		</thead>

		<?php if(have_rows('documents_categories')): ?>

			<tbody>
				<?php while(have_rows('documents_categories')): the_row(); ?>

					<?php
						$name = get_sub_field('name');
						$slug = get_sub_field('slug');
					?>
			 
				    <tr id="<?php echo $slug; ?>" class="header-row">
				    	<td class="cat-header" colspan="5"><?php echo $name; ?></td>				        
				    </tr>


					<?php
						$files = array_filter($entries, function ($entry) use ($slug) {
							return ($entry[0] == $slug); }
						);

						if(!empty($files)): ?>

							<?php
								foreach ($files as $file): 
									$date = $file[1];
									$title = $file[2];
									$author = $file[3];
									$file = $file[4];
							?>

								<tr class="file">
									<td class="cat"><?php echo $name; ?></td>
									<td class="date"><?php echo $date; ?></td>
									<td class="title">
										<?php if($file !== ''): ?>
											<a href="<?php echo $base_url; ?><?php echo $file; ?>" rel="external"><?php echo $title; ?></a>
										<?php else: ?>
											<?php echo $title; ?>
										<?php endif; ?>											
									</td>
									<td class="author"><?php echo $author; ?></td>
									<td class="pdf">
										<?php if($file !== ''): ?>
											<a href="<?php echo $base_url; ?><?php echo $file; ?>" rel="external">PDF</a>
										<?php endif; ?>											
									</td>									
								</tr>

							<?php endforeach; ?>

						<?php else: ?>


						<?php endif; ?>

				<?php endwhile; ?>

			</tbody>

		<?php endif; ?>

	</table>
</div>