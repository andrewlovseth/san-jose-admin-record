(function ($, window, document, undefined) {

	$(document).ready(function($) {

		// rel="external"
		$('a[rel="external"]').click( function() {
			window.open( $(this).attr('href') );
			return false;
		});


		// SmoothScroll to document category
		$('.documents-categories a').smoothScroll();


		// Live Filter/Search for documents with keywords
		$('#documents-filter').on('keyup', function() {
			var value = $(this).val().toLowerCase();

			$("#documents-table tr.file").filter(function() {
				$(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
			});
		});


		// Disable filter button
		$('.filter button').on('click', function(){
			return false;
		});
			
	});

})(jQuery, window, document);