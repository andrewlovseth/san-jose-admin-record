<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>

<div id="page" class="site">
	
	<header class="site-header grid">

		<div class="site-logo">
			<a href="<?php echo site_url('/'); ?>">
				<img src="<?php $image = get_field('site_logo', 'options'); echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />
			</a>
		</div>

		<div class="site-title">
			<h1><?php echo get_field('project_name', 'options'); ?></h1>
			<h2><?php echo get_field('site_name', 'options'); ?></h2>
		</div>

	</header>

	<main class="site-content">	
