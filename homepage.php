<?php

/*
	Template Name: Home
*/

get_header(); ?>

	<?php get_template_part('template-parts/homepage/intro'); ?>

	<?php get_template_part('template-parts/homepage/filter'); ?>

	<?php get_template_part('template-parts/homepage/documents'); ?>

<?php get_footer(); ?>